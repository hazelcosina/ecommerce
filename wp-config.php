<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'testsite' );
define('WP_MEMORY_LIMIT', '256M');
/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', '127.0.0.1:3305' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'e.gAR] YX6R%y+xUD]8;nh,G*3V3DAu##jx2O79}8z<CAM< SkmYWIWn$IS&Vlkn' );
define( 'SECURE_AUTH_KEY',  '4I{OfE![v<s[SRY)DlS]>[0gI^c?6#/X!TQkP6^[7B=An N 8&$jxY3|eoI<#E6t' );
define( 'LOGGED_IN_KEY',    'hoLO|`O#67n6+O2t#/7Y(7KB]bW,H=9>WFj5B{FxN/t@WJXe*&#NRX]=%veqr!]L' );
define( 'NONCE_KEY',        '.OK;+2`]^j%NRHl9J`Gdf7.!LZ_oY6QJtuOH-:B7bX0zOZy|0U&8Ro$WD~h~T(?[' );
define( 'AUTH_SALT',        'QZdtjSo<a0}drytWYbojG,yUG2h9&cugy_`sPKBSs9p$PG=pkfA^0dr2k{T@w=e=' );
define( 'SECURE_AUTH_SALT', 'c],G24O1MaS7q5ezh5oo0NwY([/18P%a)Mwy^^ytP=mgW#MaH|-gr#nk}t@t/Bl)' );
define( 'LOGGED_IN_SALT',   'qoLY]R3U0/:4$C3X/)1>%waFrF~% lCIY%SWq3.5dC!K2u, E$FU@7ZPiVHT;;r^' );
define( 'NONCE_SALT',       'n#WSC!ADt=:y}]YGoII8B<Kv|`XUf?EbtK5{{4kl]f0H)hK#9!tlg3fVx(@?!;jo' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
